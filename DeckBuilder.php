<?php
//db connection
include_once('dbconnect.php');


//-----LETS GO LETS GO LETS GO-----
if(isset($_GET['colors'])
    && isset($_GET['curve'])
    && isset($_GET['creatureCount']))
{
    //set variables
    $colors = $_GET['colors'];
    $curve = $_GET['curve'];

    //creatureCount setter
    $creatureCount = 20;
    switch($_GET['creatureCount'])
    {
        case("low"):
            $creatureCount = 16;
            break;
        case("average"):
            $creatureCount = 20;
            break;
        case("high"):
            $creatureCount = 24;
            break;
    }

    //create deck
    $lands = getLands($dbconnect);
    $cards = getCards($colors, $dbconnect);
    $deck = constructDeck($cards, $lands, $curve, $creatureCount);

    //output headers so that the file is downloaded rather than displayed
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=deck.csv');

    //create csv file
    $file = fopen("php://output", "w");

    fputcsv($file, array("Count", "Name"));
    foreach ($deck as $c)
    {
        fputcsv($file, array(1, $c['name']));
    }
    fclose($file);
}
else
{
    //do nothing
    header("Location: index.html");
}
//-----STOP-----


//get all basic lands
function getLands($dbconnect)
{
    //get lands
    $getLands = mysqli_query($dbconnect,
        "SELECT * FROM `cards`
        WHERE `name` = 'Plains'
        OR `name` = 'Swamp'
        OR `name` = 'Island'
        OR `name` = 'Mountain'
        OR `name` = 'Forest'")
    or die (mysqli_error($getLands));

    //fill array with data from query
    $lands = array();
    while ($row = mysqli_fetch_assoc($getLands))
    {
        $row_array['id'] = $row['id'];
        $row_array['name'] = str_replace("#", "\"", str_replace("*", "'", $row['name']));
        $row_array['manaCost'] = $row['manaCost'];
        $row_array['symbolCount']['W'] = substr_count($row['manaCost'], "W");
        $row_array['symbolCount']['B'] = substr_count($row['manaCost'], "B");
        $row_array['symbolCount']['U'] = substr_count($row['manaCost'], "U");
        $row_array['symbolCount']['R'] = substr_count($row['manaCost'], "R");
        $row_array['symbolCount']['G'] = substr_count($row['manaCost'], "G");
        $row_array['cmc'] = $row['cmc'];
        $row_array['colors'] = explode(',', $row['colors']);
        $row_array['types'] = explode(',', str_replace("*'", "'", $row['types']));

        //push values in array
        array_push($lands, $row_array);
    }

    //return
    return $lands;
}

//get cards with chosen colors
function getCards($chosenColors, $dbconnect)
{
    //create array with unwanted colors
    $unwantedColors = array("White", "Black", "Blue", "Red", "Green");
    foreach($chosenColors as $c)
    {
        switch($c)
        {
            case("White"):
                unset($unwantedColors[0]);
                break;
            case("Black"):
                unset($unwantedColors[1]);
                break;
            case("Blue"):
                unset($unwantedColors[2]);
                break;
            case("Red"):
                unset($unwantedColors[3]);
                break;
            case("Green"):
                unset($unwantedColors[4]);
                break;
        }
    }

    //get data with color modifier with query
    $query =
        "SELECT * FROM `cards`
        WHERE `colors` LIKE 'Colorless'".
        (in_array("White", $chosenColors) ? "OR `colors` LIKE '%White%'" : "").
        (in_array("Black", $chosenColors) ? "OR `colors` LIKE '%Black%'" : "").
        (in_array("Blue", $chosenColors) ? "OR `colors` LIKE '%Blue%'" : "").
        (in_array("Red", $chosenColors) ? "OR `colors` LIKE '%Red%'" : "").
        (in_array("Green", $chosenColors) ? "OR `colors` LIKE '%Green%'" : "");

    $getData = mysqli_query($dbconnect, $query)
    or die(mysqli_error($getData));

    //fill array with data from query
    $data = array();
    while ($row = mysqli_fetch_assoc($getData))
    {
        $row_array['id'] = $row['id'];
        $row_array['name'] = str_replace("#", "\"", str_replace("*", "'", $row['name']));
        $row_array['manaCost'] = $row['manaCost'];
        $row_array['symbolCount']['W'] = substr_count($row['manaCost'], "W");
        $row_array['symbolCount']['B'] = substr_count($row['manaCost'], "B");
        $row_array['symbolCount']['U'] = substr_count($row['manaCost'], "U");
        $row_array['symbolCount']['R'] = substr_count($row['manaCost'], "R");
        $row_array['symbolCount']['G'] = substr_count($row['manaCost'], "G");
        $row_array['cmc'] = $row['cmc'];
        $row_array['colors'] = explode(',', $row['colors']);
        $row_array['types'] = explode(',', str_replace("*'", "'", $row['types']));

        //push values in array
        array_push($data, $row_array);
    }

    //create new array to filter unwanted colors and sort on cmc
    $cmc1 = array(array(), array());
    $cmc2 = array(array(), array());
    $cmc3 = array(array(), array());
    $cmc4 = array(array(), array());
    $cmc5 = array(array(), array());
    foreach($data as $c)
    {
        if(
            (in_array("White", $unwantedColors) ? !in_array('White', $c['colors']) : true)
            && (in_array("Black", $unwantedColors) ? !in_array('Black', $c['colors']) : true)
            && (in_array("Blue", $unwantedColors) ? !in_array('Blue', $c['colors']) : true)
            && (in_array("Red", $unwantedColors) ? !in_array('Red', $c['colors']) : true)
            && (in_array("Green", $unwantedColors) ? !in_array('Green', $c['colors']) : true)
        )
        {
            switch(true)
            {
                case($c['cmc'] == 1 && in_array("Creature", $c['types'])):
                    array_push($cmc1[0], $c);
                    break;
                case($c['cmc'] == 1 && !in_array("Creature", $c['types'])):
                    array_push($cmc1[1], $c);
                    break;

                case($c['cmc'] == 2 && in_array("Creature", $c['types'])):
                    array_push($cmc2[0], $c);
                    break;
                case($c['cmc'] == 2 && !in_array("Creature", $c['types'])):
                    array_push($cmc2[1], $c);
                    break;

                case($c['cmc'] == 3 && in_array("Creature", $c['types'])):
                    array_push($cmc3[0], $c);
                    break;
                case($c['cmc'] == 3 && !in_array("Creature", $c['types'])):
                    array_push($cmc3[1], $c);
                    break;

                case($c['cmc'] == 4 && in_array("Creature", $c['types'])):
                    array_push($cmc4[0], $c);
                    break;
                case($c['cmc'] == 4 && !in_array("Creature", $c['types'])):
                    array_push($cmc4[1], $c);
                    break;

                case($c['cmc'] >= 5 && in_array("Creature", $c['types'])):
                    array_push($cmc5[0], $c);
                    break;
                case($c['cmc'] >= 5 && !in_array("Creature", $c['types'])):
                    array_push($cmc5[1], $c);
                    break;
            }
        }
    }

    //convert to single array
    $cards = array($cmc1, $cmc2, $cmc3, $cmc4, $cmc5);

    //return
    return $cards;
}

//construct a random deck with the given cards
function constructDeck($cards, $lands, $curve, $creatureCount)
{
    //tot == 36 (average)
    $bracketCount[0]['min'] = 0;
    $bracketCount[0]['max'] = 4;

    $bracketCount[1]['min'] = 2;
    $bracketCount[1]['max'] = 8;

    $bracketCount[2]['min'] = 4;
    $bracketCount[2]['max'] = 10;

    $bracketCount[3]['min'] = 2;
    $bracketCount[3]['max'] = 8;

    $bracketCount[4]['max'] = 6;

    //manaCurve
    switch($curve)
    {
        case("low"):
            $bracketCount[0]['min'] = 4;
            $bracketCount[0]['max'] = 8;

            $bracketCount[1]['min'] = 4;
            $bracketCount[1]['max'] = 10;

            $bracketCount[2]['min'] = 2;
            $bracketCount[2]['max'] = 8;

            $bracketCount[3]['max'] = 0;
            $bracketCount[3]['max'] = 6;

            $bracketCount[4]['max'] = 4;
            break;

        case("average"):
            $bracketCount[0]['min'] = 0;
            $bracketCount[0]['max'] = 4;

            $bracketCount[1]['min'] = 2;
            $bracketCount[1]['max'] = 8;

            $bracketCount[2]['min'] = 4;
            $bracketCount[2]['max'] = 10;

            $bracketCount[3]['min'] = 2;
            $bracketCount[3]['max'] = 8;

            $bracketCount[4]['max'] = 6;
            break;

        case("high"):
            $bracketCount[0]['min'] = 0;
            $bracketCount[0]['max'] = 2;

            $bracketCount[1]['min'] = 2;
            $bracketCount[1]['max'] = 6;

            $bracketCount[2]['min'] = 2;
            $bracketCount[2]['max'] = 8;

            $bracketCount[3]['min'] = 0;
            $bracketCount[3]['max'] = 10;

            $bracketCount[4]['max'] = 10;
            break;
    }

    //creature count calculator
    //bracket1
    $ccCmc1 = rand($bracketCount[0]['min'], $bracketCount[0]['max']);
    $creatureCount = $creatureCount - $ccCmc1;

    //bracket2
    if($creatureCount > 0)
    {
        $ccCmc2 = rand($bracketCount[1]['min'], (($creatureCount < $bracketCount[1]['max']) ? $creatureCount : $bracketCount[1]['max']));
        $creatureCount = $creatureCount - $ccCmc2;
    }
    else
    {
        $ccCmc2 = 0;
    }

    //bracket3
    if($creatureCount > 0)
    {
        $ccCmc3 = rand($bracketCount[2]['min'], (($creatureCount < $bracketCount[2]['max']) ? $creatureCount : $bracketCount[2]['max']));
        $creatureCount = $creatureCount - $ccCmc3;
    }
    else
    {
        $ccCmc3 = 0;
    }

    //bracket4
    if($creatureCount > 0)
    {
        $ccCmc4 = rand($bracketCount[3]['min'], (($creatureCount < $bracketCount[3]['max']) ? $creatureCount : $bracketCount[3]['max']));
        $creatureCount = $creatureCount - $ccCmc4;
    }
    else
    {
        $ccCmc4 = 0;
    }

    //bracket5+
    if ($creatureCount > 0)
    {
        $ccCmc5 = (($creatureCount < $bracketCount[4]['max']) ? $creatureCount : $bracketCount[4]['max']);
    }
    else
    {
        $ccCmc5 = 0;
    }

    //construct deck
    $deck = array();

    //bracket1
    for($i = 0; $i < $ccCmc1; $i++)
    {
        $rnd = rand(0, (count($cards[0][0])-1));
        array_push($deck, $cards[0][0][$rnd]);
    }
    if($ccCmc1 != $bracketCount[0]['max'])
    {
        for($i = $ccCmc1; $i < $bracketCount[0]['max']; $i++)
        {
            $rnd = rand(0, (count($cards[0][1])-1));
            array_push($deck, $cards[0][1][$rnd]);
        }
    }

    //bracket2
    for($i = 0; $i < $ccCmc2; $i++)
    {
        $rnd = rand(0, (count($cards[1][0])-1));
        array_push($deck, $cards[1][0][$rnd]);
    }
    if($ccCmc2 != $bracketCount[1]['max'])
    {
        for($i = $ccCmc2; $i < $bracketCount[1]['max']; $i++)
        {
            $rnd = rand(0, (count($cards[1][1])-1));
            array_push($deck, $cards[1][1][$rnd]);
        }
    }

    //bracket3
    for($i = 0; $i < $ccCmc3; $i++)
    {
        $rnd = rand(0, (count($cards[2][0])-1));
        array_push($deck, $cards[2][0][$rnd]);
    }
    if($ccCmc3 != $bracketCount[2]['max'])
    {
        for($i = $ccCmc3; $i < $bracketCount[2]['max']; $i++)
        {
            $rnd = rand(0, (count($cards[2][1])-1));
            array_push($deck, $cards[2][1][$rnd]);
        }
    }

    //bracket4
    for($i = 0; $i < $ccCmc4; $i++)
    {
        $rnd = rand(0, (count($cards[3][0])-1));
        array_push($deck, $cards[3][0][$rnd]);
    }
    if($ccCmc4 != $bracketCount[3]['max'])
    {
        for($i = $ccCmc4; $i < $bracketCount[3]['max']; $i++)
        {
            $rnd = rand(0, (count($cards[3][1])-1));
            array_push($deck, $cards[3][1][$rnd]);
        }
    }

    //bracket5+
    if ($ccCmc5 != 0)
    {
        for($i = 0; $i < $ccCmc5; $i++)
        {
            $rnd = rand(0, (count($cards[4][0])-1));
            array_push($deck, $cards[4][0][$rnd]);
        }
        if($ccCmc5 != $bracketCount[4]['max'])
        {
            for($i = $ccCmc5; $i < $bracketCount[4]['max']; $i++)
            {
                $rnd = rand(0, (count($cards[4][1])-1));
                array_push($deck, $cards[4][1][$rnd]);
            }
        }
    }
    else
    {
        for($i = 0; $i < $bracketCount[4]['max']; $i++)
        {
            $rnd = rand(0, (count($cards[4][1])-1));
            array_push($deck, $cards[4][1][$rnd]);
        }
    }

    //add lands
    $deck = addLands($deck, $lands);

    //return
    return $deck;
}

//add lands to deck
function addLands($deck, $lands)
{
    //set counters to 0
    $deckW = 0;
    $deckB = 0;
    $deckU = 0;
    $deckR = 0;
    $deckG = 0;

    //count
    foreach($deck as $card)
    {
        if($card['symbolCount']['W'] > 0)
        {
            $deckW = $deckW + $card['symbolCount']['W'];
        }
        if($card['symbolCount']['B'] > 0)
        {
            $deckB = $deckB + $card['symbolCount']['B'];
        }
        if($card['symbolCount']['U'] > 0)
        {
            $deckU = $deckU + $card['symbolCount']['U'];
        }
        if($card['symbolCount']['R'] > 0)
        {
            $deckR = $deckR + $card['symbolCount']['R'];
        }
        if($card['symbolCount']['G'] > 0)
        {
            $deckG = $deckG + $card['symbolCount']['G'];
        }
    }

    //land calculations
    $landCount = 24;
    $landW = round($landCount * ($deckW / ($deckW + $deckB + $deckU + $deckR + $deckG)));
    $landB = round($landCount * ($deckB / ($deckW + $deckB + $deckU + $deckR + $deckG)));
    $landU = round($landCount * ($deckU / ($deckW + $deckB + $deckU + $deckR + $deckG)));
    $landR = round($landCount * ($deckR / ($deckW + $deckB + $deckU + $deckR + $deckG)));
    $landG = round($landCount * ($deckG / ($deckW + $deckB + $deckU + $deckR + $deckG)));

    //add lands to deck
    foreach($lands as $l)
    {
        switch($l['name'])
        {
            case("Plains"):
                for ($i = 0; $i < $landW; $i++)
                {
                    array_push($deck, $l);
                }
                break;
            case("Swamp"):
                for ($i = 0; $i < $landB; $i++)
                {
                    array_push($deck, $l);
                }
                break;
            case("Island"):
                for ($i = 0; $i < $landU; $i++)
                {
                    array_push($deck, $l);
                }
                break;
            case("Mountain"):
                for ($i = 0; $i < $landR; $i++)
                {
                    array_push($deck, $l);
                }
                break;
            case("Forest"):
                for ($i = 0; $i < $landG; $i++)
                {
                    array_push($deck, $l);
                }
                break;
        }
    }

    //return deck with lands
    return $deck;
}