<?php

//db connection
include_once('dbconnect.php');

//get lands
$getLands = mysqli_query($dbconnect,
    "SELECT * FROM `cards`
    WHERE `name` = 'Plains'
    OR `name` = 'Swamp'
    OR `name` = 'Island'
    OR `name` = 'Mountain'
    OR `name` = 'Forest'")
or die (mysqli_error($getLands));

//fill array with data from query
$lands = array();
while ($row = mysqli_fetch_assoc($getLands))
{
    $row_array['id'] = $row['id'];
    $row_array['name'] = str_replace("#", "\"", str_replace("*", "'", $row['name']));
    $row_array['manaCost'] = $row['manaCost'];
    $row_array['symbolCount']['W'] = substr_count($row['manaCost'], "W");
    $row_array['symbolCount']['B'] = substr_count($row['manaCost'], "B");
    $row_array['symbolCount']['U'] = substr_count($row['manaCost'], "U");
    $row_array['symbolCount']['R'] = substr_count($row['manaCost'], "R");
    $row_array['symbolCount']['G'] = substr_count($row['manaCost'], "G");
    $row_array['cmc'] = $row['cmc'];
    $row_array['colors'] = explode(',', $row['colors']);
    $row_array['types'] = explode(',', str_replace("*'", "'", $row['types']));

    //push values in array
    array_push($lands, $row_array);
}


//get data with color modifier with query
$getData = mysqli_query($dbconnect,
    "SELECT * FROM `cards`
    WHERE `colors` LIKE '%Blue%'
    OR `colors` LIKE '%Black%'
    OR `colors` LIKE 'Colorless'")
or die(mysqli_error($getData));

//fill array with data from query
$data = array();
while ($row = mysqli_fetch_assoc($getData))
{
    $row_array['id'] = $row['id'];
    $row_array['name'] = str_replace("#", "\"", str_replace("*", "'", $row['name']));
    $row_array['manaCost'] = $row['manaCost'];
    $row_array['symbolCount']['W'] = substr_count($row['manaCost'], "W");
    $row_array['symbolCount']['B'] = substr_count($row['manaCost'], "B");
    $row_array['symbolCount']['U'] = substr_count($row['manaCost'], "U");
    $row_array['symbolCount']['R'] = substr_count($row['manaCost'], "R");
    $row_array['symbolCount']['G'] = substr_count($row['manaCost'], "G");
    $row_array['cmc'] = $row['cmc'];
    $row_array['colors'] = explode(',', $row['colors']);
    $row_array['types'] = explode(',', str_replace("*'", "'", $row['types']));

    //push values in array
    array_push($data, $row_array);
}

//create new array to filter unwanted colors and sort on cmc
$cmc1 = array(array(), array());
$cmc2 = array(array(), array());
$cmc3 = array(array(), array());
$cmc4 = array(array(), array());
$cmc5 = array(array(), array());
foreach($data as $c)
{
    if(in_array('Red', $c['colors']) == false
    && in_array('Green', $c['colors']) == false
    && in_array('White', $c['colors']) == false)
    {
        switch(true)
        {
            case($c['cmc'] == 1 && in_array("Creature", $c['types'])):
                array_push($cmc1[0], $c);
                break;
            case($c['cmc'] == 1 && !in_array("Creature", $c['types'])):
                array_push($cmc1[1], $c);
                break;

            case($c['cmc'] == 2 && in_array("Creature", $c['types'])):
                array_push($cmc2[0], $c);
                break;
            case($c['cmc'] == 2 && !in_array("Creature", $c['types'])):
                array_push($cmc2[1], $c);
                break;

            case($c['cmc'] == 3 && in_array("Creature", $c['types'])):
                array_push($cmc3[0], $c);
                break;
            case($c['cmc'] == 3 && !in_array("Creature", $c['types'])):
                array_push($cmc3[1], $c);
                break;

            case($c['cmc'] == 4 && in_array("Creature", $c['types'])):
                array_push($cmc4[0], $c);
                break;
            case($c['cmc'] == 4 && !in_array("Creature", $c['types'])):
                array_push($cmc4[1], $c);
                break;

            case($c['cmc'] >= 5 && in_array("Creature", $c['types'])):
                array_push($cmc5[0], $c);
                break;
            case($c['cmc'] >= 5 && !in_array("Creature", $c['types'])):
                array_push($cmc5[1], $c);
                break;
        }
    }
}

//creature count calc
//min = 2+2+2+2+6 = 14
//max = $creatureCount
$creatureCount = 22;

//bracket1
$ccCmc1 = rand(2, 8);
$creatureCount = $creatureCount - $ccCmc1;

//bracket2
$ccCmc2 = rand(2, (($creatureCount < 8) ? $creatureCount : 8));
$creatureCount = $creatureCount - $ccCmc2;

//bracket3
$ccCmc3 = rand(2, (($creatureCount < 8) ? $creatureCount : 8));
$creatureCount = $creatureCount - $ccCmc3;

//bracket4
$ccCmc4 = rand(2, (($creatureCount < 8) ? $creatureCount : 8));
$creatureCount = $creatureCount - $ccCmc4;

//bracket5+
if ($creatureCount > 0)
{
    $ccCmc5 = (($creatureCount < 6 ) ? $creatureCount : 6);
}
else
{
    $ccCmc5 = 0;
}

//construct deck
$deck = array();
//bracket1
for($i = 0; $i < $ccCmc1; $i++)
{
    $rnd = rand(0, (count($cmc1[0])-1));
    array_push($deck, $cmc1[0][$rnd]);
}
if($ccCmc1 != 8)
{
    for($i = $ccCmc1; $i < 8; $i++)
    {
        $rnd = rand(0, (count($cmc1[1])-1));
        array_push($deck, $cmc1[1][$rnd]);
    }
}

//bracket2
for($i = 0; $i < $ccCmc2; $i++)
{
    $rnd = rand(0, (count($cmc2[0])-1));
    array_push($deck, $cmc2[0][$rnd]);
}
if($ccCmc2 != 8)
{
    for($i = $ccCmc2; $i < 8; $i++)
    {
        $rnd = rand(0, (count($cmc2[1])-1));
        array_push($deck, $cmc2[1][$rnd]);
    }
}

//bracket3
for($i = 0; $i < $ccCmc3; $i++)
{
    $rnd = rand(0, (count($cmc3[0])-1));
    array_push($deck, $cmc3[0][$rnd]);
}
if($ccCmc3 != 8)
{
    for($i = $ccCmc3; $i < 8; $i++)
    {
        $rnd = rand(0, (count($cmc3[1])-1));
        array_push($deck, $cmc3[1][$rnd]);
    }
}

//bracket4
for($i = 0; $i < $ccCmc4; $i++)
{
    $rnd = rand(0, (count($cmc4[0])-1));
    array_push($deck, $cmc4[0][$rnd]);
}
if($ccCmc4 != 8)
{
    for($i = $ccCmc4; $i < 8; $i++)
    {
        $rnd = rand(0, (count($cmc4[1])-1));
        array_push($deck, $cmc4[1][$rnd]);
    }
}

//bracket5+
if ($ccCmc5 != 0)
{
    for($i = 0; $i < $ccCmc5; $i++)
    {
        $rnd = rand(0, (count($cmc5[0])-1));
        array_push($deck, $cmc5[0][$rnd]);
    }
    if($ccCmc5 != 6)
    {
        for($i = $ccCmc5; $i < 6; $i++)
        {
            $rnd = rand(0, (count($cmc5[1])-1));
            array_push($deck, $cmc5[1][$rnd]);
        }
    }
}
else
{
    for($i = 0; $i < 6; $i++)
    {
        $rnd = rand(0, (count($cmc5[1])-1));
        array_push($deck, $cmc5[1][$rnd]);
    }
}

//add Lands
$deckW = 0;
$deckB = 0;
$deckU = 0;
$deckR = 0;
$deckG = 0;
foreach($deck as $card)
{
    if($card['symbolCount']['W'] > 0)
    {
        $deckW = $deckW + $card['symbolCount']['W'];
    }
    if($card['symbolCount']['B'] > 0)
    {
        $deckB = $deckB + $card['symbolCount']['B'];
    }
    if($card['symbolCount']['U'] > 0)
    {
        $deckU = $deckU + $card['symbolCount']['U'];
    }
    if($card['symbolCount']['R'] > 0)
    {
        $deckR = $deckR + $card['symbolCount']['R'];
    }
    if($card['symbolCount']['G'] > 0)
    {
        $deckG = $deckG + $card['symbolCount']['G'];
    }
}

//land calculations
$landW = round(22 * ($deckW / ($deckW + $deckB + $deckU + $deckR + $deckG)));
$landB = round(22 * ($deckB / ($deckW + $deckB + $deckU + $deckR + $deckG)));
$landU = round(22 * ($deckU / ($deckW + $deckB + $deckU + $deckR + $deckG)));
$landR = round(22 * ($deckR / ($deckW + $deckB + $deckU + $deckR + $deckG)));
$landG = round(22 * ($deckG / ($deckW + $deckB + $deckU + $deckR + $deckG)));

//add lands to deck
foreach($lands as $l)
{
    switch($l['name'])
    {
        case("Plains"):
            for ($i = 0; $i < $landW; $i++)
            {
                array_push($deck, $l);
            }
            break;
        case("Swamp"):
            for ($i = 0; $i < $landB; $i++)
            {
                array_push($deck, $l);
            }
            break;
        case("Island"):
            for ($i = 0; $i < $landU; $i++)
            {
                array_push($deck, $l);
            }
            break;
        case("Mountain"):
            for ($i = 0; $i < $landR; $i++)
            {
                array_push($deck, $l);
            }
            break;
        case("Forest"):
            for ($i = 0; $i < $landG; $i++)
            {
                array_push($deck, $l);
            }
            break;
    }
}


//echo to check
echo(json_encode($deck));


?>